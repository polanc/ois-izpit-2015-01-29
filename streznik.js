var express = require('express'), path = require('path'), fs = require('fs');
var app = express();
app.use(express.static(__dirname + '/public'));
var podatkiSpomin = ["admin/nimda", "gost/gost"];

app.get('/api/prijava', function(req, res) {
	res.send({status: "status", napaka: "Opis napake"});
});

app.get('/prijava', function(req, res) {
	res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>Krneki</b> nima pravice prijave v sistem!</p></body></html>");
});

var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');

var podatkiSpomin = ["admin/nimda", "gost/gost"];

var podatkiDatotekaStreznik = {};

function preveriSpomin(uporabniskoIme, geslo){
	var Ime = podatkiSpomin[0].split("/");
	var Geslo = podatkiSpomin[1].split("/");
	for (var i = 0; i < Ime.length; i++){
		if (uporabniskoIme == Ime[i] && geslo == Geslo[i])
			return true;
	}
	return false;
}

function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	var Ime;
	var Geslo;
	for (var i = 0; i < Ime.length; i++){
		if (uporabniskoIme == Ime[i] && geslo == Geslo[i])
			return true;
	}
	return false;
}